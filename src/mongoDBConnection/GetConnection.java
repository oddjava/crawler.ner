package mongoDBConnection;

import org.bson.Document;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class GetConnection {

	static MongoClient mongoClient = new MongoClient( "178.32.61.54" , 27017 );
	public static DBCollection connect(String collectionName)
	{
		DB db = null;
		DBCollection coll=null;
		try{
			
	         db=mongoClient.getDB("crawler");
	         coll = db.getCollection(collectionName);
	      }catch(Exception e){
	         System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	      }
		return coll;
	}
	public static void main(String[] args) {
		GetConnection.connect("domain");
	}
}
