package nerCrawler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.jsoup.nodes.Document;

import nerCrawler.GetData;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;

import geoLocation.GeoLocation;
import mongoDBConnection.GetConnection;

public class Crawler {

	private static final int MYTHREADS = 100;
	static int count = 1;
	private static long startTime;
	static DBCollection crawldatadb;
	static DBCollection sampleCrawlDataDB;
	static DBCollection domainDB;
	static DBCollection settingsDB;
	static GetData getData;
	// static DBCollection seedUrl = crawler.getCollection("SeedUrl");
	// static DBCollection name = crawler.getCollection("nameStructure");
	// static DBCollection newdb = crawler.getCollection("newdb");

	static{
		startTime = System.currentTimeMillis();
		crawldatadb = GetConnection.connect("crawldata");
		sampleCrawlDataDB = GetConnection.connect("sampleCrawldata");
		domainDB = GetConnection.connect("domain");
		settingsDB = GetConnection.connect("settings");
		getData = new GetData();
	}
	
	public void crawlSeeds(ArrayList<localData> list) {
		ExecutorService executor = Executors.newFixedThreadPool(MYTHREADS);
		for (int i = 0; i < list.size(); i++) {
			System.out.println("Seed is " + list.get(i).url);

			String url = list.get(i).url;
			boolean status = list.get(i).status;
			String personName = list.get(i).personName;
			Runnable worker = new MyRunnable(url, status,personName);
			// executor.submit(worker);
			executor.execute(worker);
		}
		executor.shutdown();
		// Wait until all threads are finish
		while (!executor.isTerminated()) {

		}
		System.out.println("\nFinished all threads");

	}

	public static void main(String args[]) throws Exception {

		NERMultiMoreURLS24();

	}

	public static void NERMultiMoreURLS24() throws InterruptedException {
		long startTime = System.currentTimeMillis();

		ArrayList<localData> list = new ArrayList<>();
		ArrayList<localData> crawlList = new ArrayList<>();

		BasicDBObject query = new BasicDBObject();
		List<BasicDBObject> andObject = new ArrayList<BasicDBObject>();
		andObject.add(new BasicDBObject("boilerpipeStatus", "true"));
		andObject.add(new BasicDBObject("nerStatus", "false"));
		query.put("$and", andObject);
		DBCursor cursor = crawldatadb.find(query);
		int cursorSize = cursor.size();
		int cursorLimit = 0;
		int batch = 0;

		if (cursorSize >= 8) {
			System.out.println("Adding " + cursorSize + " data in list..... Please wait for some time");
			while (cursor.hasNext()) {
				DBObject object = cursor.next();

				String systemName = "";
				String link = object.get("link").toString();
				String authorityStatus = object.get("authorityStatus").toString();
				String imageStatus = object.get("imageStatus").toString();
				String translationStatus = object.get("translationStatus").toString();
				String personName = object.get("personName").toString();
			

				boolean status = false;
				if (translationStatus.contains("true") && imageStatus.contains("true")
						&& authorityStatus.contains("true")) {
					status = true;
				} else {
					status = false;
				}

				localData data = new localData(link, status, personName);
				crawlList.add(data);
				/*
				 * if (list.size() == 15) { System.out.println(
				 * "List is 15 and calling threads"); new
				 * Crawler().crawlSeeds(list); System.out.println(
				 * "Now list is clear"); list.clear(); } else { list.add(data);
				 * }
				 */
			}
		}

		if (crawlList.size() < 15) {
			System.out.println("Data is less than 15 to fetch NER");
			System.out.println("Wait for 2 minutes to get more than 15 data to start NER crawl");
			Thread.sleep(120000); // 2 minutes in milliseconds
			NERMultiMoreURLS24();
		} else {
			while (cursorLimit != crawlList.size()) {
				if (batch < 15) {
					list.add(crawlList.get(cursorLimit));
					cursorLimit++;
					batch++;
				} else {
					System.out.println("List is 15 and calling threads");
					new Crawler().crawlSeeds(list);
					System.out.println("Now list is clear");
					list.clear();
					batch = 0;
				}
			}
			if(list.size()>0)
			{
				System.out.println("Remaining links are deployed to Threads");
				new Crawler().crawlSeeds(list);
				System.out.println("Now list is clear");
				list.clear();
				batch = 0;
			}
		}

		long endTime = System.currentTimeMillis();
		System.out.println("Done with all the execution");
		long time = endTime - startTime;
		System.out.println("Program took " + time + " to execute");
		System.out.println("After half hour program will restart");
		Thread.sleep(120000); // 2 minutes in milliseconds
		NERMultiMoreURLS24();

	}

	public static class MyRunnable implements Runnable {
		private final String urlToProcess;
		private boolean status;
		private String personName;
		List<String> textsToProcess = new ArrayList<String>();
		Document doc;
		StanFordNER stanford = new StanFordNER();

		MyRunnable(String urlToProcess, boolean status,String personName) {
			this.urlToProcess = urlToProcess;
			this.status = status;
			this.personName = personName;
		}

		@Override
		public void run() {
			try {
				String content = "";
				DBCursor cursor = crawldatadb.find(new BasicDBObject("link", urlToProcess));
				while (cursor.hasNext()) {
					content = cursor.next().get("mainContent").toString();
				}

				HashMap hm = stanford.getDataForAllClasses(content);
				String leadDate = hm.get("date").toString();
				String location = hm.get("location").toString();
				String personName1 = hm.get("person").toString();
				
				String companyName = hm.get("organization").toString();

				if (leadDate.length() > 2)
					leadDate = leadDate.substring(1, leadDate.length() - 1);
				else
					leadDate = "";

				if (location.length() > 2)
					location = location.substring(1, location.length() - 1);
				else
					location = "";

				if (personName1.length() > 2)
					personName1 = personName1.substring(1, personName1.length() - 1);
				else
					personName1 = "";

				if(personName.isEmpty())
				{
					personName = personName1;
				}
				else
				{
					personName = personName+personName1;
				}
				
				if (companyName.length() > 2)
					companyName = companyName.substring(1, companyName.length() - 1);
				else
					companyName = "";

				String latlong = "";
				if (location != "") {
					latlong = GeoLocation.updateLocation(location);
				}

				BasicDBObject searchQuery = new BasicDBObject("link", urlToProcess);

				// Define the update query:
				if (status == true) {
					BasicDBObject updateFields = new BasicDBObject();
					updateFields.append("leadDate", leadDate);
					updateFields.append("location", location);
					updateFields.append("geoLocation", latlong);
					updateFields.append("personName", personName);
					updateFields.append("companyName", companyName);
					updateFields.append("status", "true");
					updateFields.append("nerStatus", "true");
					BasicDBObject setQuery = new BasicDBObject();
					setQuery.append("$set", updateFields);
					crawldatadb.update(searchQuery, setQuery);
					sampleCrawlDataDB.update(searchQuery, setQuery);
					System.out.println("Done for " + urlToProcess);
				} else {
					BasicDBObject updateFields = new BasicDBObject();
					updateFields.append("leadDate", leadDate);
					updateFields.append("location", location);
					updateFields.append("personName", personName);
					updateFields.append("companyName", companyName);
					updateFields.append("status", "false");
					updateFields.append("nerStatus", "true");
					BasicDBObject setQuery = new BasicDBObject();
					setQuery.append("$set", updateFields);
					crawldatadb.update(searchQuery, setQuery);
					sampleCrawlDataDB.update(searchQuery, setQuery);
				}

				DBObject dbObject;
				try {
					if (latlong.isEmpty()) {
						dbObject = (DBObject) JSON.parse("{'geoip' : {" + "'type' : 'MultiPoint',"
								+ "'coordinates' : [ " + "[ 0, 0 ]" + "]" + "}" + "}");
						crawldatadb.update(searchQuery, new BasicDBObject("$set", dbObject));
						sampleCrawlDataDB.update(searchQuery, new BasicDBObject("$set", dbObject));
					} else {
						// present.put("keyword", "try");
						//System.out.println("Location::" + latlong);
						// crawldataDB1.update(present, present);
						BasicDBObject doc = new BasicDBObject();
						String l[] = latlong.split("/");

						String query = "'coordinates' : [ ";
						for (int i = 0; i < l.length; i++) {
							String lon[] = l[i].split(",");
							if (i == l.length - 1) {
								query += "[" + lon[1] + "," + lon[0] + "]";
								System.out.println(lon[1] + " " + lon[0]);
							} else
								query += "[" + lon[1] + "," + lon[0] + "],";
							// doc.putAll();

							// System.out.println("check "+l[i]+" ");
						}
						query += "]";

						dbObject = (DBObject) JSON.parse("{'geoip' : {" + "'type' : 'MultiPoint'," + query + "" + "}}");
						crawldatadb.update(searchQuery, new BasicDBObject("$set", dbObject));
						sampleCrawlDataDB.update(searchQuery, new BasicDBObject("$set", dbObject));
					}
					// }
				} catch (Exception e) {
					dbObject = (DBObject) JSON.parse("{'geoip' : {" + "'type' : 'MultiPoint'," + "'coordinates' : [ "
							+ "[ 0, 0 ]" + "]" + "}" + "}");
					crawldatadb.update(searchQuery, new BasicDBObject("$set", dbObject));
					sampleCrawlDataDB.update(searchQuery, new BasicDBObject("$set", dbObject));
					System.out.println("My..." + e);

				}

			}

			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// System.out.println("List of text To Process:"+textsToProcess);
		}

	}

}

class localData {
	public String url;
	public boolean status;
	public String personName;
	public localData(String url, boolean status, String personName) {
		super();
		this.url = url;
		this.status = status;
		this.personName=personName;
	}
}